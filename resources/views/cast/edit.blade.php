@extends('layouts.master')

@section('judul')
Halaman Edit Cast : {{$cast->nama}}
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method("PUT")
        <div class="form-group">
        <label for="nama">Nama Cast :</label>
        <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" id="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="umur">Umur :</label>
            <input type="number" name="umur" value="{{$cast->umur}}" class="form-control" id="umur">
            </div>
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        <div class="form-group">
            <label for="bio">Bio :</label>
            <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Bio">
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Edit</button>
  </form>
@endsection