@extends('layouts.master')

@section('judul')
Halaman Detail Cast : {{$cast->nama}}
@endsection

@section('content')
    <h2>Show Cast {{$cast->id}}</h2>
    <h3>Nama : {{$cast->nama}}</h3>
    <h4>Umur : {{$cast->umur}}</h4>
    <p>Biografi : {{$cast->bio}}</p>
@endsection
